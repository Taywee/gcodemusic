extern crate clap;
extern crate regex;

use std::f32;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::BufWriter;
use std::fs::File;
use std::collections::HashMap;
use regex::Regex;
use clap::{Arg, App};

/** Writes note at given frequency and length, in signed short format at 44.1KHz.
 * If frequency is 0, write silence.
 */
fn write_note(writer: &mut impl Write, frequency: f32, length: f32) {
    let fadesamples = 500;
    let sample_count = (44100.0 * length) as u64;
    if length == 0.0 {
        return;
    }
    if frequency == 0.0 {
        for sample in 0..sample_count {
            writer.write(&0i16.to_be_bytes());
        }
    } else {
        for sample in 0..sample_count {
            let samplesleft = sample_count - sample;
            let amplitude = if sample < fadesamples {
                sample as f32 / fadesamples as f32
            } else if samplesleft < fadesamples {
                samplesleft as f32 / fadesamples as f32
            } else {
                1.0
            };
            let time = sample as f32 / 44100.0;
            let level = (time * frequency * 2.0 * f32::consts::PI).sin() * amplitude;
            let raw_level: i16 = (level * 32000.0) as i16;
            writer.write(&raw_level.to_be_bytes());
        }
    }
}

fn main() {
    // TODO: maybe turn this into a function instead
    let frequencies = {
        let mut frequencies: HashMap<&'static str, f32> = HashMap::new();
        frequencies.insert("c0", 16.35);
        frequencies.insert("c#0", 17.32);
        frequencies.insert("db0", 17.32);
        frequencies.insert("d0", 18.35);
        frequencies.insert("d#0", 19.45);
        frequencies.insert("eb0", 19.45);
        frequencies.insert("e0", 20.60);
        frequencies.insert("fb0", 20.60);
        frequencies.insert("f0", 21.83);
        frequencies.insert("f#0", 23.12);
        frequencies.insert("gb0", 23.12);
        frequencies.insert("g0", 24.50);
        frequencies.insert("g#0", 25.96);
        frequencies.insert("ab0", 25.96);
        frequencies.insert("a0", 27.50);
        frequencies.insert("a#0", 29.14);
        frequencies.insert("bb0", 29.14);
        frequencies.insert("b0", 30.87);
        frequencies.insert("cb1", 30.87);
        frequencies.insert("c1", 32.70);
        frequencies.insert("c#1", 34.65);
        frequencies.insert("db1", 34.65);
        frequencies.insert("d1", 36.71);
        frequencies.insert("d#1", 38.89);
        frequencies.insert("eb1", 38.89);
        frequencies.insert("e1", 41.20);
        frequencies.insert("fb1", 41.20);
        frequencies.insert("f1", 43.65);
        frequencies.insert("f#1", 46.25);
        frequencies.insert("gb1", 46.25);
        frequencies.insert("g1", 49.00);
        frequencies.insert("g#1", 51.91);
        frequencies.insert("ab1", 51.91);
        frequencies.insert("a1", 55.00);
        frequencies.insert("a#1", 58.27);
        frequencies.insert("bb1", 58.27);
        frequencies.insert("b1", 61.74);
        frequencies.insert("cb2", 61.74);
        frequencies.insert("c2", 65.41);
        frequencies.insert("c#2", 69.30);
        frequencies.insert("db2", 69.30);
        frequencies.insert("d2", 73.42);
        frequencies.insert("d#2", 77.78);
        frequencies.insert("eb2", 77.78);
        frequencies.insert("e2", 82.41);
        frequencies.insert("fb2", 82.41);
        frequencies.insert("f2", 87.31);
        frequencies.insert("f#2", 92.50);
        frequencies.insert("gb2", 92.50);
        frequencies.insert("g2", 98.00);
        frequencies.insert("g#2", 103.83);
        frequencies.insert("ab2", 103.83);
        frequencies.insert("a2", 110.00);
        frequencies.insert("a#2", 116.54);
        frequencies.insert("bb2", 116.54);
        frequencies.insert("b2", 123.47);
        frequencies.insert("cb3", 123.47);
        frequencies.insert("c3", 130.81);
        frequencies.insert("c#3", 138.59);
        frequencies.insert("db3", 138.59);
        frequencies.insert("d3", 146.83);
        frequencies.insert("d#3", 155.56);
        frequencies.insert("eb3", 155.56);
        frequencies.insert("e3", 164.81);
        frequencies.insert("fb3", 164.81);
        frequencies.insert("f3", 174.61);
        frequencies.insert("f#3", 185.00);
        frequencies.insert("gb3", 185.00);
        frequencies.insert("g3", 196.00);
        frequencies.insert("g#3", 207.65);
        frequencies.insert("ab3", 207.65);
        frequencies.insert("a3", 220.00);
        frequencies.insert("a#3", 233.08);
        frequencies.insert("bb3", 233.08);
        frequencies.insert("b3", 246.94);
        frequencies.insert("cb4", 246.94);
        frequencies.insert("c4", 261.63);
        frequencies.insert("c#4", 277.18);
        frequencies.insert("db4", 277.18);
        frequencies.insert("d4", 293.66);
        frequencies.insert("d#4", 311.13);
        frequencies.insert("eb4", 311.13);
        frequencies.insert("e4", 329.63);
        frequencies.insert("fb4", 329.63);
        frequencies.insert("f4", 349.23);
        frequencies.insert("f#4", 369.99);
        frequencies.insert("gb4", 369.99);
        frequencies.insert("g4", 392.00);
        frequencies.insert("g#4", 415.30);
        frequencies.insert("ab4", 415.30);
        frequencies.insert("a4", 440.00);
        frequencies.insert("a#4", 466.16);
        frequencies.insert("bb4", 466.16);
        frequencies.insert("b4", 493.88);
        frequencies.insert("cb5", 493.88);
        frequencies.insert("c5", 523.25);
        frequencies.insert("c#5", 554.37);
        frequencies.insert("db5", 554.37);
        frequencies.insert("d5", 587.33);
        frequencies.insert("d#5", 622.25);
        frequencies.insert("eb5", 622.25);
        frequencies.insert("e5", 659.25);
        frequencies.insert("fb5", 659.25);
        frequencies.insert("f5", 698.46);
        frequencies.insert("f#5", 739.99);
        frequencies.insert("gb5", 739.99);
        frequencies.insert("g5", 783.99);
        frequencies.insert("g#5", 830.61);
        frequencies.insert("ab5", 830.61);
        frequencies.insert("a5", 880.00);
        frequencies.insert("a#5", 932.33);
        frequencies.insert("bb5", 932.33);
        frequencies.insert("b5", 987.77);
        frequencies.insert("cb6", 987.77);
        frequencies.insert("c6", 1046.50);
        frequencies.insert("c#6", 1108.73);
        frequencies.insert("db6", 1108.73);
        frequencies.insert("d6", 1174.66);
        frequencies.insert("d#6", 1244.51);
        frequencies.insert("eb6", 1244.51);
        frequencies.insert("e6", 1318.51);
        frequencies.insert("fb6", 1318.51);
        frequencies.insert("f6", 1396.91);
        frequencies.insert("f#6", 1479.98);
        frequencies.insert("gb6", 1479.98);
        frequencies.insert("g6", 1567.98);
        frequencies.insert("g#6", 1661.22);
        frequencies.insert("ab6", 1661.22);
        frequencies.insert("a6", 1760.00);
        frequencies.insert("a#6", 1864.66);
        frequencies.insert("bb6", 1864.66);
        frequencies.insert("b6", 1975.53);
        frequencies.insert("cb7", 1975.53);
        frequencies.insert("c7", 2093.00);
        frequencies.insert("c#7", 2217.46);
        frequencies.insert("db7", 2217.46);
        frequencies.insert("d7", 2349.32);
        frequencies.insert("d#7", 2489.02);
        frequencies.insert("eb7", 2489.02);
        frequencies.insert("e7", 2637.02);
        frequencies.insert("fb7", 2637.02);
        frequencies.insert("f7", 2793.83);
        frequencies.insert("f#7", 2959.96);
        frequencies.insert("gb7", 2959.96);
        frequencies.insert("g7", 3135.96);
        frequencies.insert("g#7", 3322.44);
        frequencies.insert("ab7", 3322.44);
        frequencies.insert("a7", 3520.00);
        frequencies.insert("a#7", 3729.31);
        frequencies.insert("bb7", 3729.31);
        frequencies.insert("b7", 3951.07);
        frequencies.insert("cb8", 3951.07);
        frequencies.insert("c8", 4186.01);
        frequencies.insert("c#8", 4434.92);
        frequencies.insert("db8", 4434.92);
        frequencies.insert("d8", 4698.63);
        frequencies.insert("d#8", 4978.03);
        frequencies.insert("eb8", 4978.03);
        frequencies.insert("e8", 5274.04);
        frequencies.insert("fb8", 5274.04);
        frequencies.insert("f8", 5587.65);
        frequencies.insert("f#8", 5919.91);
        frequencies.insert("gb8", 5919.91);
        frequencies.insert("g8", 6271.93);
        frequencies.insert("g#8", 6644.88);
        frequencies.insert("ab8", 6644.88);
        frequencies.insert("a8", 7040.00);
        frequencies.insert("a#8", 7458.62);
        frequencies.insert("bb8", 7458.62);
        frequencies.insert("b8", 7902.13);
        frequencies
    };

    let tempo_regex = Regex::new(r"(?P<bpm>\d+)/(?P<beatnote>\d+)").unwrap();
    let note_regex = Regex::new(r"(?P<note>[a-g][#b]?[0-9]+),(?P<subdivision>[0-9]+(?:\.[0-9]+)?)(?P<silencemod>.)?").unwrap();
    let matches = App::new("df-elevation-to-model")
        .version("0.1")
        .author("Taylor C. Richberger <taywee@gmx.com>")
        .about("A simple generator that allows inputting a music description language and outputting gcode")
        .arg(Arg::with_name("input")
             .short("i")
             .long("input")
             .value_name("FILE")
             .required(true)
             .help("Specify input music file.")
             .takes_value(true))
        .arg(Arg::with_name("raw")
             .short("r")
             .long("raw")
             .value_name("FILE")
             .help("Output pcm16be file.")
             .takes_value(true))
        .arg(Arg::with_name("gcode")
             .short("g")
             .long("gcode")
             .value_name("FILE")
             .help("Output gcode file.")
             .takes_value(true))
        .get_matches();
    let input_name = matches.value_of("input").unwrap();
    let raw_name = matches.value_of("raw");
    let gcode_name = matches.value_of("gcode");
    let input = File::open(input_name).unwrap();
    let mut reader = BufReader::new(input);
    let mut tempo = String::new();
    reader.read_line(&mut tempo).unwrap();
    let tempo = tempo.trim();
    let tempo_captures = tempo_regex.captures(tempo).unwrap();
    let bpm: f32 = tempo_captures["bpm"].parse().unwrap();
    let beatnote: f32 = tempo_captures["beatnote"].parse().unwrap();
    let whole_per_minute = bpm / beatnote;

    // This is how long a whole note lasts.  We can use this to calculate note duration
    let seconds_per_whole = 60.0 / whole_per_minute;

    let mut raw_output = raw_name.map(|name| BufWriter::new(File::create(name).unwrap()));
    let mut gcode_output = gcode_name.map(|name| BufWriter::new(File::create(name).unwrap()));

    for line in reader.lines() {
        let l = line.unwrap();
        let comment_index = l.find(';');
        let code = match comment_index {
            Some(i) => l.split_at(i).0,
            None => l.as_str(),
        };
        for note in code.split_whitespace() {
            let note_captures = note_regex.captures(note).unwrap();
            let note = &note_captures["note"];
            let frequency = frequencies.get(note).unwrap();
            let subdivision: f32 = note_captures["subdivision"].parse().unwrap();
            let note_length = seconds_per_whole / subdivision;
            let note_part = match note_captures.name("silencemod") {
                Some(m) => match m.as_str() {
                    "s" => note_length,
                    "." => note_length * 0.5,
                    _ => panic!("Could not process duration"),
                },
                _ => note_length * 0.9,
            };
            let silence_part = note_length - note_part;
            if let Some(ref mut output) = raw_output {
                write_note(output, *frequency, note_part);
                if silence_part > 0.0 {
                    write_note(output, 0.0, silence_part);
                }
            }
            if let Some(ref mut output) = gcode_output {
                // Note part
                output.write(&format!("M300 S{} P{}\n", *frequency as u16, (note_part * 1000.0) as u16).into_bytes());
                // Rest part
                if silence_part > 0.0 {
                    output.write(&format!("M300 S0 P{}\n", (silence_part * 1000.0) as u16).into_bytes());
                }
            }
        }
    }
}
